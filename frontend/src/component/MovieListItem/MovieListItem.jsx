import React from 'react';

import './MovieListItem.css';
import {Link} from "react-router-dom";

function MovieListItem(props) {
    let description = props.description;
    let maxDescriptionLength = 150;
    if (description.length > maxDescriptionLength){
        description = description.substring(0,maxDescriptionLength).concat(" ... ");
    }
    let title = props.title;
    let maxTitleLength = 45;
    if (title.length > maxTitleLength){
        title = title.substring(0,maxTitleLength).concat(" ... ");
    }
    let movieGenre = props.genresByIds.map(genre => {
        if(genre.id === props.genre)
            return genre.name;
    });

    return (
        <Link to={"/movieDetails/" + props.id} className="card-wrapper">
            <img className="movie-image" src={props.image} sizes="auto" alt="NotFound"/>
            <div className="description-content">
                <h5>{title}</h5>
                <p>{movieGenre}</p>
                <p>{description}</p>
            </div>
        </Link>
    )
}

export default MovieListItem;