import React, {Fragment} from 'react';
import StarRatingComponent from "react-star-rating-component";

function CommentListItem(props) {
    return (
        <Fragment key={props.index}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-2 text-left">
                        <img className="comment-avatar"
                             src="https://png.pngtree.com/svg/20161027/service_default_avatar_182956.png"
                             alt="avatar"
                        />
                        <h4>{props.userName}</h4>
                    </div>
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="col text-left">
                                <StarRatingComponent
                                    name="commentRating"
                                    value={props.rating}
                                    starCount={10}
                                    editing={false}
                                />
                            </div>
                            <div className="col text-right">
                                <p>{props.timeOfComment}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col text-left">
                                <p>{props.comment}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
        </Fragment>
    )

}

export default CommentListItem;