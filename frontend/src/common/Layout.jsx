import React from 'react';
import {Route, Switch} from 'react-router-dom';
import MovieList from "../containers/MovieList/MovieList";
import MovieDetails from "../containers/MovieDetails/MovieDetails";

function Layout() {
    return (
        <div>
            <div className="container jumbotron">
                <Switch>
                    <Route path="/" exact component={MovieList}/>
                    <Route path="/movieDetails/:id" exact component={MovieDetails}/>
                </Switch>
            </div>
        </div>
    )
}

export default Layout;