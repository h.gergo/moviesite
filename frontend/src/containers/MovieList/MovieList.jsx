import React, {Component, Fragment} from 'react';
import axios from 'axios/index';
import MovieListItem from "../../component/MovieListItem/MovieListItem";
import "./MovieList.css"

class MovieList extends Component {

    state = {
        movies: [],
        search: "",
        genres: []
    };

    componentDidMount() {
        const apiKey = "?api_key=84b2674dcba2f54328f0fef9d7abfe44";
        for (let i = 1; i < 6; i++) {
            axios.get("https://api.themoviedb.org/3/movie/top_rated" + apiKey + "&page=" + i)
                .then(response => {
                    let updatedMovies = [...this.state.movies];
                    updatedMovies.push(...response.data.results);
                    this.setState({movies: updatedMovies})
                })
                .catch(error => console.warn(error))
        }

        axios.get("https://api.themoviedb.org/3/genre/movie/list" + apiKey)
            .then(response => {
                this.setState({genres: response.data.genres})
            })
    };

    eventChangeHandler = (event) => {
        this.setState({search: event.target.value});
    };

    render() {

        const baseUrl = "http://image.tmdb.org/t/p/";
        const imageSize = "w300/";

        let filteredMovies = this.state.movies.filter(
            movie => {
                return movie.title.toLowerCase()
                    .indexOf(this.state.search.toLowerCase()) !== -1
            }
        );

        let movies = filteredMovies.map(movie => (
            <Fragment key={movie.id}>
                <MovieListItem
                    id={movie.id}
                    image={baseUrl+imageSize+movie.poster_path}
                    title={movie.title}
                    description={movie.overview}
                    genre={movie.genre_ids[0]}
                    genresByIds={this.state.genres}
                />
            </Fragment>
        ));
        return (
            <div>
                <div>
                    <label htmlFor="search">Search:</label>
                    <input
                        id="search"
                        type="text"
                        name="search"
                        className="search-bar"
                        placeholder="Enter a movie title"
                        value={this.state.search}
                        onChange={this.eventChangeHandler}
                    />
                </div>
                <div>
                    {movies}
                </div>
            </div>
        )
    }
}

export default MovieList;