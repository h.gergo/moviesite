import React, {Fragment} from 'react';
import axios from "axios";
import Popup from "reactjs-popup";
import StarRatingComponent from "react-star-rating-component";

import "./MovieDetails.css";
import CommentListItem from "../../component/CommentListItem/CommentListItem";


class MovieDetails extends React.Component {

    state = {
        movieDetails: [],
        movieGenres: [],
        productionCompany: "",
        movieCast: [],
        movieCrew: [],
        director: "",
        comments: [],
        commentForm: {
            userName: "",
            comment: "",
            rating: 0
        }
    };

    componentWillMount() {
        const apiKey = "?api_key=84b2674dcba2f54328f0fef9d7abfe44";
        const baseUrl = "https://api.themoviedb.org/3/movie/";
        const localHost = 'http://localhost:8080/api/comment/';

        axios.all([
            axios.get(baseUrl + this.props.match.params.id + apiKey),
            axios.get(baseUrl + this.props.match.params.id + "/credits" + apiKey),
            axios.get(localHost + this.props.match.params.id)
        ])
            .then(axios.spread((details, cast, comment) => {
                this.setState({
                    movieDetails: details.data,
                    movieGenres: details.data.genres,
                    productionCompany: details.data.production_companies[0].name,
                    movieCast: cast.data.cast.slice(0, 5),
                    movieCrew: cast.data.crew,
                    comments: comment.data
                })
            }))
            .then(() => {
                this.state.movieCrew.map(crewMember => {
                    if (crewMember.job === "Director")
                        this.setState({director: crewMember.name})
                })
            })
            .catch(console.warn);
    };

    addNewComment = () => {
        let data = this.state.commentForm;
        axios.post('http://localhost:8080/api/comment/' + this.props.match.params.id, data)
            .then(response => {
                console.log(response)
            })
            .catch(console.warn)
    };

    inputChangeHandler = (event) => {
        const target = event.target;
        const updatedForm = {...this.state.commentForm};

        updatedForm[target.name] = target.value;

        this.setState({commentForm: updatedForm});
    };

    onStarClick = (nextValue) => {
        const updatedForm = {...this.state.commentForm};

        updatedForm.rating = nextValue;
        this.setState({commentForm: updatedForm})
    };

    openNewCommentModal = () => {
        this.setState({openNewCommentModal: true})
    };
    closeNewCommentModal = () => {
        this.setState({openNewCommentModal: false})
    };

    goBack = () => {
        this.props.history.push('/');
    };

    render() {
        const baseUrl = "http://image.tmdb.org/t/p/";
        const imageSize = "w300/";
        const profileImage = "w45/";
        let sum = 0;
        let commentList = this.state.comments;
        commentList.map(comment => {
            sum += comment.rating;
        });

        let avgRating = (sum / commentList.length).toFixed(1);

        let cast = this.state.movieCast
            .map(member =>
                <Fragment key={member.id}>
                    <tr>
                        <td><img src={baseUrl + profileImage + member.profile_path} alt="test"/></td>
                        <td>{member.character}</td>
                        <td>{member.name}</td>
                    </tr>
                </Fragment>
            );

        let movieGenres = this.state.movieGenres.map(genre =>
            <Fragment key={genre.id}>
                <span> - {genre.name} </span>
            </Fragment>
        );

        let comments = commentList.map((comment, index) =>
            <CommentListItem
                key={index}
                index={index}
                comment={comment.comment}
                rating={comment.rating}
                timeOfComment={comment.timeOfComment}
                userName={comment.userName}
            />
        );
        let movieDetails = this.state.movieDetails;
        let commentForm = this.state.commentForm;
        document.title = movieDetails.title;

        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 text-left">
                        <img src={baseUrl + imageSize + movieDetails.poster_path} alt=""/>
                        <h5>Production company</h5>
                        <p>{this.state.productionCompany}</p>
                        <h5>Director</h5>
                        <p>{this.state.director}</p>
                    </div>
                    <div className="col-lg-8">
                        <div className="row">
                            <div className="col-sm-8 text-left">
                                <h4>{movieDetails.title}</h4>
                                <br/>
                            </div>
                            <div className="col-sm-4 text-right">
                                <button name="goBack" className={"btn btn-primary"} type="button" onClick={this.goBack}>
                                    Go Back
                                </button>
                                <br/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6 text-left">
                                <p>Release date: {movieDetails.release_date}</p>
                                <p>Genres: {movieGenres} </p>
                            </div>
                            <div className="col-sm-6 text-right">
                                <p>Avg. ratings: {(commentList.length === 0) ? "Not rated" : avgRating} / 10 </p>
                                <p>Runtime: {movieDetails.runtime} mins</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 text-center">
                                <p>{movieDetails.overview}</p>
                                <table className="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Image</th>
                                        <th scope="col">Role</th>
                                        <th scope="col">Actor / Actress</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {cast}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <Popup
                            className="modal-content"
                            closeOnDocumentClick
                            modal
                            open={this.state.openNewCommentModal}
                            onClose={this.closeNewCommentModal}
                        >
                            <div className="popup">
                                <form onSubmit={this.addNewComment}>
                                    <h4>Add new comment</h4>
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Enter a name for your comment"
                                        name="userName"
                                        value={commentForm.userName}
                                        onChange={this.inputChangeHandler}

                                    />
                                    <br/>
                                    <textarea cols="60" rows="8"
                                              type="text"
                                              className="form-control"
                                              name="comment"
                                              onChange={this.inputChangeHandler}
                                              value={commentForm.comment}
                                              placeholder="Enter your comment here"
                                              required
                                    />
                                    <br/>

                                    <div>
                                        <h5>Rating: </h5>
                                        <StarRatingComponent
                                            className="star-rating"
                                            name="rating"
                                            value={commentForm.rating}
                                            starCount={10}
                                            editing={true}
                                            onStarClick={this.onStarClick.bind(this)}
                                        />

                                    </div>
                                    <br/>
                                    <div align="center">
                                        <button type="button"
                                                className="btn btn-danger"
                                                onClick={this.closeNewCommentModal}
                                        >Cancel
                                        </button>
                                        <button name="addNewComment" type="submit" className="btn btn-primary">Add new comment</button>
                                    </div>
                                </form>
                            </div>

                        </Popup>
                    </div>
                </div>
                <div>
                    <h3>Comments ({commentList.length})</h3>
                    <button onClick={this.openNewCommentModal} name="newComment" type="button" className="btn btn-primary">Add
                        new comment
                    </button>
                    <hr/>
                </div>
                <div>
                    {comments}
                </div>
            </div>
        )
    }
}

export default MovieDetails;