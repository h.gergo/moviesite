package com.movies.repository;

import com.movies.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query(value = "SELECT * FROM comments WHERE movie_id = :id ORDER BY timeOfComment DESC ", nativeQuery = true)
    Optional<List<Comment>> findAllByMovieId(@Param("id")Long id);
}
