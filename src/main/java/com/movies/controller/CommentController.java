package com.movies.controller;

import com.movies.dto.CommentToList;
import com.movies.dto.CommentToSave;
import com.movies.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<CommentToList>> getAllComments(@PathVariable ("id") Long id){
        return new ResponseEntity<>(commentService.getComments(id), HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> saveComment(@RequestBody CommentToSave commentToSave, @PathVariable Long id){
        commentService.saveComment(commentToSave,id);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
