package com.movies.service;

import com.movies.domain.Comment;
import com.movies.dto.CommentToList;
import com.movies.dto.CommentToSave;
import com.movies.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CommentService {

    private CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public List<CommentToList> getComments(Long id) {
        Optional<List<Comment>> allCommentForMovie = commentRepository.findAllByMovieId(id);
        if (allCommentForMovie.isPresent()){
            return allCommentForMovie.get()
                    .stream()
                    .map(CommentToList::new)
                    .collect(Collectors.toList());
        } else {
            throw new EntityNotFoundException("No movie found with given id: " + id);
        }
    }

    public Comment saveComment(CommentToSave commentToSave, Long movieId) {
        Comment comment = new Comment();
        comment.setComment(commentToSave.getComment());
        comment.setName(commentToSave.getUserName());
        comment.setRating(commentToSave.getRating());
        comment.setTimeOfComment(LocalDateTime.now());
        comment.setMovie_id(movieId);

        return commentRepository.save(comment);
    }
}
