package com.movies.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.movies.domain.Comment;

import java.time.LocalDateTime;

public class CommentToList {

    private String userName;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm")
    private LocalDateTime timeOfComment;
    private String comment;
    private Integer rating;

    public CommentToList(Comment comment) {
        this.userName = comment.getName();
        this.timeOfComment = comment.getTimeOfComment();
        this.comment = comment.getComment();
        this.rating = comment.getRating();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDateTime getTimeOfComment() {
        return timeOfComment;
    }

    public void setTimeOfComment(LocalDateTime timeOfComment) {
        this.timeOfComment = timeOfComment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
