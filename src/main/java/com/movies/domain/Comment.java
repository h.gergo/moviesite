package com.movies.domain;

import com.movies.dto.CommentToSave;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name ="comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Column(columnDefinition="text")
    private String comment;
    private LocalDateTime timeOfComment;
    private Integer rating;

    private Long movie_id;

    public Comment() {
    }

    public Comment(CommentToSave commentToSave) {
        this.name = commentToSave.getUserName();
        this.comment = commentToSave.getComment();
        this.timeOfComment = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getTimeOfComment() {
        return timeOfComment;
    }

    public void setTimeOfComment(LocalDateTime timeOfComment) {
        this.timeOfComment = timeOfComment;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(Long movie_id) {
        this.movie_id = movie_id;
    }
}
