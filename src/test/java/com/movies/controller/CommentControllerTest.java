package com.movies.controller;

import com.movies.domain.Comment;
import com.movies.dto.CommentToList;
import com.movies.service.CommentService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
public class CommentControllerTest {

    private MockMvc mockMvc;

    @Mock
    private CommentService commentServiceMock;

    @InjectMocks
    private CommentController commentController;

    @BeforeEach
    public void setup () {
        mockMvc = MockMvcBuilders.standaloneSetup(commentController).build();
    }

    @AfterEach
    public void validate() {validateMockitoUsage();}

    @Test
    public void testGetAllComments () throws Exception {
        Comment comment1 = new Comment();
        comment1.setTimeOfComment(LocalDateTime.now());
        comment1.setRating(8);
        comment1.setName("test1");
        comment1.setMovie_id(1L);
        comment1.setComment("this is a test comment");

        Comment comment2 = new Comment();
        comment2.setTimeOfComment(LocalDateTime.now());
        comment2.setRating(6);
        comment2.setName("test2");
        comment2.setMovie_id(1L);
        comment2.setComment("this is another test comment");

        List<CommentToList> testComments = Arrays.asList(comment1,comment2).stream()
                .map(CommentToList::new).collect(Collectors.toList());

        when(commentServiceMock.getComments(1L)).thenReturn(testComments);

        this.mockMvc.perform(get("/api/comment/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].userName", is("test1")))
                .andExpect(jsonPath("$[0].rating", is(8)))
                .andExpect(jsonPath("$[0].comment", is("this is a test comment")))

                .andExpect(jsonPath("$[1].userName", is("test2")))
                .andExpect(jsonPath("$[1].userName", is("test2")))
                .andExpect(jsonPath("$[1].comment", is("this is another test comment")))
        ;

        verify(commentServiceMock, times(1)).getComments(1L);
    }
}
