package com.movies.selenium.test;

import com.movies.selenium.environment.EnvironmentManager;
import com.movies.selenium.environment.RunEnvironment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MakeCommentTest {

    @BeforeEach
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
    }

    @Test
    public void demo(){
        WebDriver webDriver = RunEnvironment.getWebDriver();

        webDriver.get("http://localhost:3000");

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        webDriver.findElement(By.cssSelector("input[name='search']")).sendKeys("shaw");
        String search = webDriver.findElement(By.cssSelector("input[name='search']")).getAttribute("value");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assertions.assertEquals(search,"shaw");
        webDriver.findElement(By.cssSelector("img.movie-image")).click();


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title = webDriver.findElement(By.cssSelector("h4")).getAttribute("innerHTML");
        Assertions.assertEquals(title,"The Shawshank Redemption");

        webDriver.findElement(By.cssSelector("button[name='newComment']")).click();
        webDriver.findElement(By.cssSelector("input[name='userName']")).sendKeys("seleniumTest");
        webDriver.findElement(By.cssSelector("textarea[name='comment']")).sendKeys("seleniumComment");
        webDriver.findElement(By.cssSelector("button[type='submit']")).click();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void tearDown(){
        EnvironmentManager.shutDownDriver();
    }
}
