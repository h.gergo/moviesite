package com.movies.repository;

import com.movies.config.SpringWebConfig;
import com.movies.config.TestConfiguration;
import com.movies.domain.Comment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {SpringWebConfig.class, TestConfiguration.class})
public class CommentRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Test
    public void testFindAllByMovieId () {
        Comment comment1 = new Comment();
        comment1.setTimeOfComment(LocalDateTime.now());
        comment1.setRating(8);
        comment1.setName("test1");
        comment1.setMovie_id(1L);
        comment1.setComment("this is a test comment");

        Comment comment2 = new Comment();
        comment2.setTimeOfComment(LocalDateTime.now());
        comment2.setRating(6);
        comment2.setName("test2");
        comment2.setMovie_id(1L);
        comment2.setComment("this is another test comment");

        commentRepository.save(comment1);
        commentRepository.save(comment2);

        Optional<List<Comment>> comments = commentRepository.findAllByMovieId(1L);
        Comment testComment = comments.get().get(1);

        assertEquals(comments.get().size(),2);
        assertEquals(testComment.getComment(),comment2.getComment());
        assertEquals(testComment.getRating(),comment2.getRating());
        assertEquals(testComment.getName(),comment2.getName());
    }
}
