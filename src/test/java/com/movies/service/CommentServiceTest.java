package com.movies.service;

import com.movies.domain.Comment;
import com.movies.dto.CommentToList;
import com.movies.dto.CommentToSave;
import com.movies.repository.CommentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CommentServiceTest {

    private CommentRepository commentRepositoryMock;
    private CommentService commentService;

    @BeforeEach
    public void setup(){
        commentRepositoryMock = mock(CommentRepository.class);
        commentService = new CommentService(commentRepositoryMock);
    }

    @Test
    public void testGetComments() {
        Comment comment1 = new Comment();
        comment1.setTimeOfComment(LocalDateTime.now());
        comment1.setRating(8);
        comment1.setName("test1");
        comment1.setMovie_id(1L);
        comment1.setComment("this is a test comment");

        Comment comment2 = new Comment();
        comment2.setTimeOfComment(LocalDateTime.now());
        comment2.setRating(6);
        comment2.setName("test2");
        comment2.setMovie_id(1L);
        comment2.setComment("this is another test comment");

        List<Comment> commentsMock = new ArrayList<>();
        commentsMock.add(comment1);
        commentsMock.add(comment2);

        when(commentRepositoryMock.findAllByMovieId(1L)).thenReturn(Optional.of(commentsMock));

        List<CommentToList> testComments = commentService.getComments(1L);
        CommentToList testCommentToList = testComments.get(1);

        assertEquals(testComments.size(),commentsMock.size());
        assertEquals(testCommentToList.getComment(),comment2.getComment());
    }

    @Test
    public void testSaveComment() {
        Comment comment1 = new Comment();
        comment1.setTimeOfComment(LocalDateTime.now());
        comment1.setRating(8);
        comment1.setName("test1");
        comment1.setMovie_id(1L);
        comment1.setComment("this is a test comment");

        CommentToSave commentToSave1 = new CommentToSave();
        commentToSave1.setComment(comment1.getComment());
        commentToSave1.setRating(comment1.getRating());
        commentToSave1.setUserName(comment1.getName());

        when(commentRepositoryMock.save(any(Comment.class))).thenAnswer(returnsFirstArg());

        Comment testComment = commentService.saveComment(commentToSave1,1L);

        assertEquals(comment1.getName(),testComment.getName());
        assertEquals(comment1.getRating(),testComment.getRating());
        assertEquals(comment1.getComment(),testComment.getComment());
    }
}
